# RoboCar #

This is the python code required for getting up a raspberry powered remote controlled car.

### What is robocar? ###

* RoboCar - A raspberry pi powered car
* Version 1.0
* [Complete Blog @ ConfusedCoders](http://www.confusedcoders.com/random/how-to-create-a-raspberry-pi-powered-car-remote-controlled-via-mobile-phone)

### How do I get set up? ###

* [Get started with a raspberry pi](http://www.confusedcoders.com/random/getting-started-with-the-raspberry-pi)
* [SSH into Raspberry pi and enable wifi](http://www.confusedcoders.com/random/ssh-into-a-raspberry-pi-and-connect-pi-to-wifi)
* Setup Hardware stuffs - Check blog for steps and images
* Python packages


### Who do I talk to? ###

* Send me comments on ConfusedCoders.com